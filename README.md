# Google Pubsub Prometheus Exporter

[![build status](https://gitlab.com/Teddy-Schmitz/pubsub-exporter/badges/master/build.svg)](https://gitlab.com/Teddy-Schmitz/pubsub-exporter/commits/master)

Exports metrics from Stackdriver into Prometheus for Google Pubsub

**This project should be considered alpha and the api is subject to breakage**

## Config

Provide Google Project ID/Name as an environment variable

``
GCP_PROJECT_ID
``

## Usage

Just run the binary or container and it will check every minute for updated statistics and provide them on port 4987