package main

import (
	"context"
	"net/http"
	"time"

	monitoring "cloud.google.com/go/monitoring/apiv3"

	"log"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/teddy-schmitz/pubsub-exporter/metrics"
)

func main() {

	r := http.NewServeMux()
	r.Handle("/metrics", promhttp.Handler())

	go http.ListenAndServe(":4987", r)

	ctx := context.Background()
	c, err := monitoring.NewMetricClient(ctx)
	if err != nil {
		log.Fatalln(err)
	}
	defer c.Close()

	metrics.GetMetrics(c)

	t := time.NewTicker(1 * time.Minute)
	for _ = range t.C {
		metrics.GetMetrics(c)
	}
}
