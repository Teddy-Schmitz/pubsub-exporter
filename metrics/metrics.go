package metrics

import (
	"fmt"
	"time"

	"context"

	monitoring "cloud.google.com/go/monitoring/apiv3"
	googlepb "github.com/golang/protobuf/ptypes/timestamp"
	"github.com/prometheus/client_golang/prometheus"
	monitoringpb "google.golang.org/genproto/googleapis/monitoring/v3"
)

type Metric struct {
	Subscription string
	Value        int64
	Name         string
}

var QueueOutstandingMessages = prometheus.NewGaugeVec(prometheus.GaugeOpts{
	Name: "pubsub_outstanding_messages_total",
	Help: "How many messages have been delivered but not acked",
}, []string{"sub_name"})

var QueueUndeliveredMessages = prometheus.NewGaugeVec(prometheus.GaugeOpts{
	Name: "pubsub_undelivered_messages_total",
	Help: "How many messages sitting in the queue (Backlog)",
}, []string{"sub_name"})

var QueueOldestUnackedMessages = prometheus.NewGaugeVec(prometheus.GaugeOpts{
	Name: "pubsub_oldest_unacked_message_seconds",
	Help: "Age of the oldest unacknowledged message in the queue",
}, []string{"sub_name"})

var metricList = map[string]*prometheus.GaugeVec{
	"subscription/num_outstanding_messages":   QueueOutstandingMessages,
	"subscription/num_undelivered_messages":   QueueUndeliveredMessages,
	"subscription/oldest_unacked_message_age": QueueOldestUnackedMessages,
}

func GetMetrics(c *monitoring.MetricClient) []*Metric {
	ctx := context.Background()
	m := make([]*Metric, 0, len(metricList))
	for name, gauge := range metricList {
		req := &monitoringpb.ListTimeSeriesRequest{
			Name:   monitoring.MetricProjectPath(ProjectID),
			Filter: fmt.Sprintf(`metric.type = "pubsub.googleapis.com/%s"`, name),
			View:   monitoringpb.ListTimeSeriesRequest_FULL,
			Interval: &monitoringpb.TimeInterval{
				StartTime: &googlepb.Timestamp{
					Seconds: time.Now().Add(-1 * time.Minute).Unix(),
				},
				EndTime: &googlepb.Timestamp{
					Seconds: time.Now().Unix(),
				},
			},
		}

		it := c.ListTimeSeries(ctx, req)

		for {
			resp, err := it.Next()
			if err != nil {
				fmt.Println(err)
				break
			}

			p := resp.Points[0].Value.GetInt64Value()
			gauge.WithLabelValues(resp.Resource.Labels["subscription_id"]).Set(float64(p))
			m = append(m, &Metric{resp.Resource.Labels["subscription_id"], p, name})
		}
	}
	return m
}

func init() {
	prometheus.MustRegister(QueueOutstandingMessages)
	prometheus.MustRegister(QueueUndeliveredMessages)
	prometheus.MustRegister(QueueOldestUnackedMessages)
}
