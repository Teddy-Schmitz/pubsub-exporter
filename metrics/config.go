package metrics

import "os"

func envString(e, d string) string {
	s := os.Getenv(e)
	if s == "" {
		return d
	}
	return s
}

var ProjectID = envString("GCP_PROJECT_ID", "dev")
